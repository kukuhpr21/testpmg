package com.kukuh.testpmg.service.product;

import com.kukuh.testpmg.entity.Product;

import java.util.List;

public interface ProductService {

    void save(Product product);

    Product get(Long id);

    void delete(Long id);

    List<Product> list();
}

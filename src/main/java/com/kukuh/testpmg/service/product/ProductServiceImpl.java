package com.kukuh.testpmg.service.product;

import com.kukuh.testpmg.entity.Product;
import com.kukuh.testpmg.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepo repo;


    @Override
    public void save(Product product) {
        product.setProductId( UUID.randomUUID().toString());
        repo.save(product);
    }

    @Override
    public Product get(Long id) {
        Product product = repo.getById(id);
        return product;
    }

    @Override
    public void delete(Long id) {
        Product product = repo.getById(id);
        repo.delete(product);
    }

    @Override
    public List<Product> list() {
        return repo.findAllByOrderByIdDesc();
    }
}

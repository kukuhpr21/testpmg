package com.kukuh.testpmg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.kukuh.testpmg")
@EntityScan("com.kukuh.testpmg")
public class TestPmgApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestPmgApplication.class, args);
    }

}

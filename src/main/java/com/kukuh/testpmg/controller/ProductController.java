package com.kukuh.testpmg.controller;

import com.kukuh.testpmg.entity.Product;
import com.kukuh.testpmg.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.UUID;

@Controller
public class ProductController {

    @Autowired
    private ProductService service;

    @GetMapping(value = "/")
    public String index() {
        return "home";
    }

    @GetMapping(value = "/products")
    public String products(Model model) {
        List<Product> products = service.list();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping(value = "/new")
    public String newProduct(Model model) {
        model.addAttribute("product", new Product());
        return "new_product";
    }

    @PostMapping(value = "/save")
    public String save(@ModelAttribute("product") Product product) {
        service.save(product);
        return "redirect:/products";
    }

    @PostMapping(value = "/update")
    public String update(@ModelAttribute("product") Product product) {
        service.save(product);
        return "redirect:/products";
    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView edit(@PathVariable(name = "id") Long id) {
        ModelAndView mav = new ModelAndView("edit_product");
        Product product = service.get(id);
        mav.addObject("product", product);
        return mav;
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        service.delete(id);
        return "redirect:/products";
    }
}

package com.kukuh.testpmg.repository;

import com.kukuh.testpmg.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long> {
    List<Product> findAllByOrderByIdDesc();
}

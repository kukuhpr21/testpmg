CREATE TABLE `product` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `product_id` varchar(100) NOT NULL,
                           `name` varchar(100) NOT NULL,
                           `description` varchar(200) DEFAULT NULL,
                           `type` varchar(50) NOT NULL,
                           `category` varchar(50) NOT NULL,
                           `price` double(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
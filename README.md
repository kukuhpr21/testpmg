# Aplikasi Test PMG

Aplikasi WEB CRUD Product untuk Test PMG:

## Fitur - fitur :
- [Home](#home)
- [Product List](#product-list)
- [Create Product](#create-product)


## Home
Menampilkan halaman utama dengan logo spring boot
![img.png](img.png)

## Product List
Menampilkan daftar product dalam bentuk tabel dan didalamnya terdapat fitur edit dan delete
![img_1.png](img_1.png)
![img_2.png](img_2.png)

## Create Product
Menampilakan form untuk menambahkan product seperti :
- name
- description
- type
- category
- price

![img_3.png](img_3.png)
FROM openjdk:11.0-jdk-oraclelinux8

COPY target/test-pmg-0.0.1-SNAPSHOT.jar /app/application.jar

CMD ["java", "-jar", "/app/application.jar"]